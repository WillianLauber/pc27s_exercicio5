/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

import java.util.Random;

public class Producer implements Runnable {

    private final static Random generator = new Random();
    private final Buffer [] buffer;    
            
    public Producer( Buffer [] shared ){
        buffer = shared;        
        
    }
    
    @Override
    public void run() {
        int [] sum = new int[buffer.length];
        
        for (int count =1; count <=10; count++){
            
            try {
                //Dorme, atribui um valor no Buffer e soma
                //Thread.sleep(generator.nextInt(3000));
                for(int i=0; i<buffer.length; i++){
                    buffer[i].set( count );
                    sum[i] +=count; //incrementa a soma de valores
                    System.out.printf("\t%2d\n", sum[i]);
                }
            } catch ( InterruptedException e){
                e.printStackTrace();
            }
        }
                    System.out.printf("\nFinalizando produtor.");
    }

    
    
}
