/**
 * Exemplo de Produtor/Consumidor
 * sem sincronizacao
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Exemplo5  {

    
    public static void main(String [] args){
        
        //cria novo pool de threads com duas threads
        ExecutorService application = Executors.newCachedThreadPool();
        
        int num = 2;
        Buffer [] buffer = new BufferSincronizado[num];
        for(int i=0; i<num; i++){
            buffer[i] = new BufferSincronizado();
        }
        
        System.out.println("Ação\t\tSoma do Produtor\tSoma do Consumidor");;
        System.out.println("-------\t\t-----------------\t------------\n");
        
        //Executa Producer e Consumer, fornecendo-lhes acesso ao mesmo Buffer->BufferNaoSincronizado: buffer        
        application.execute( new Producer( buffer ) );
        
        for(int i=0; i<num; i++){
            application.execute( new Consumer( buffer, i ) );
        }
                    
        application.shutdown();
    }//fim main
    
}//fim classe
