/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
