/**
 * BufferSincronizado
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

public class BufferSincronizado implements Buffer {

    private int buffer = -1; //Compartilhado pelas threads producer e consumer
    
    public synchronized void set (int value ) throws InterruptedException{
        
        System.out.printf("\nProdutor escreve\t%2d", value);
        buffer = value;
    };
    
    public synchronized int get () throws InterruptedException{
        System.out.printf("\nConsumidor lê   \t%2d", buffer);
        return buffer;
    }
    
}
