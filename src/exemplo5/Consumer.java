/**
 * Buffer
 * 
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 16/08/2017
 */
package exemplo5;

import java.util.Random;

public class Consumer implements Runnable {

    private final static Random generator = new Random();
    private final Buffer [] buffer;
    private int indice;
    
    public Consumer( Buffer [] shared, int indice ){
        buffer = shared;
        
        this.indice = indice;
    }        
    
    @Override
    public void run() {
        int sum=0;
                
        for (int count =1; count <=10; count++){
            
            try {
                //Dorme, adquire um valor do Buffer e soma ele (nao atribui nada no Buffer compartilhado)
                Thread.sleep(generator.nextInt(4000));
                
                //for(int i=0; i<buffer.length; i++){
                    sum += buffer[indice].get();
                    System.out.printf("\t%2d\n", sum);
                                
                    buffer[indice].set(buffer[indice].get()-1);
                //}
                
            } catch ( InterruptedException e){
                e.printStackTrace();
            }            
    }
        System.out.printf("\n%s %d\n%s\n",
                    "Consumidor leu valores totalizando: ", sum, "Finalizando consumidor");            
        }

    
    
}
